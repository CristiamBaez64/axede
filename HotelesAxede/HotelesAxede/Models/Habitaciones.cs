﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

#nullable disable

namespace HotelesAxede.Models
{
    public partial class Habitaciones
    {
        public Habitaciones()
        {
            Reservas = new HashSet<Reservas>();
        }

        public int IdHabitacion { get; set; }
        public int? IdSucursal { get; set; }
        public int? IdTipoAlojamiento { get; set; }
        public decimal? CupoMax { get; set; }
        public decimal? ValorPorPersona { get; set; }
        public bool? Disponible { get; set; }

        public virtual Sucursales IdSucursalNavigation { get; set; }
        public virtual TipoAlojamiento IdTipoAlojamientoNavigation { get; set; }
        public virtual ICollection<Reservas> Reservas { get; set; }
    }
}