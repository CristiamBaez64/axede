﻿using HotelesAxede.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HotelesAxede.Controllers
{
    public class HomeController : Controller
    {
        //Conexion a la base de datos
        HotelesaxedeContext context = new HotelesaxedeContext();

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        //Traer datos de habitaciones disponibles
        [HttpGet]
        public IActionResult InfoHabitacionesDisponibles()
        {
            var Habitaciones = (from h in context.Habitaciones
                                from s in context.Sucursales
                                from a in context.TipoAlojamiento
                                where h.IdSucursal == s.IdSucursal && h.IdTipoAlojamiento == a.IdTipo && h.Disponible == true
                                select new
                                {
                                    numHabitacion = h.IdHabitacion,
                                    sucursal = s.Sucursal,
                                    alojamiento = a.Tipo,
                                    valor = h.ValorPorPersona,
                                    cupo = h.CupoMax
                                }).ToArray();
            return Json(new { data = Habitaciones });
        }

        //Traer datos de habitaciones reservadas
        [HttpGet]
        public IActionResult InfoHabitacionesReservadas()
        {
            var Habitaciones = (from h in context.Habitaciones
                                from s in context.Sucursales
                                from a in context.TipoAlojamiento
                                where h.IdSucursal == s.IdSucursal && h.IdTipoAlojamiento == a.IdTipo && h.Disponible == false
                                select new
                                {
                                    numHabitacion = h.IdHabitacion,
                                    sucursal = s.Sucursal,
                                    alojamiento = a.Tipo,
                                    valor = h.ValorPorPersona,
                                    cupo = h.CupoMax
                                }).ToArray();
            return Json(new { data = Habitaciones });
        }

        //Traer la informacion de los selects
        [HttpPost]
        public JsonResult TraerInfoSelects()
        {
            return Json(new
            {
                success = true,
                sucursales = context.Sucursales.ToArray(),
                tiposalojamiento = context.TipoAlojamiento.ToArray()
            });
        }

        //Metodo para guardar una nueva habitacion


        [HttpPost]
        //Metodo para guardar la habitacion
        public IActionResult saveHabitacion(int Sucursal, int TipoAlojamiento, decimal CupoMaximo, decimal ValPersona)
        {
            var guardar = new Habitaciones();
            guardar.IdSucursal = Sucursal;
            guardar.IdTipoAlojamiento = TipoAlojamiento;
            guardar.CupoMax = CupoMaximo;
            guardar.ValorPorPersona = ValPersona;
            guardar.Disponible = true;
            context.Habitaciones.Add(guardar);
            context.SaveChanges();

            return Json(new { success = true });
        }

    }
}
