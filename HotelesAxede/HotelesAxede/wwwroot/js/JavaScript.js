﻿$(document).ready(function () {
    dataTableDisponibles = $('#tableDisponibles').DataTable({
        "ajax": {
            "url": "/Home/InfoHabitacionesDisponibles",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "numHabitacion" },
            { "data": "sucursal" },
            { "data": "alojamiento" },
            { "data": "valor" },
            { "data": "cupo" }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });

    dataTableReservadas = $('#tableReservadas').DataTable({
        "ajax": {
            "url": "/Home/InfoHabitacionesReservadas",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "numHabitacion" },
            { "data": "sucursal" },
            { "data": "alojamiento" },
            { "data": "valor" },
            { "data": "cupo" }
        ],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
        }
    });
});

//Abre modal para agregar habitacion
function addHabitacion() {
    $("#FormularioCrearHabitacion")[0].reset();
    /*Ajax para traer info selects*/
    $.ajax({
        type: "POST",
        url: "/Home/TraerInfoSelects",
        success: function (response) {
            if (response.success) {
                //Select Sucursal
                $('#Sucursal').html("").append(`
                    <option value="">-- Elija --</option>
                `);
                $.each(response.sucursales, function (key, value) {
                    $('#Sucursal').append(`
                        <option value="${value.idSucursal}">${value.sucursal}</option>
                    `);
                });
                //Select Tipo Alojamiento
                $('#TipoAlojamiento').html("").append(`
                    <option value="">-- Elija --</option>
                `);
                $.each(response.tiposalojamiento, function (key, value) {
                    $('#TipoAlojamiento').append(`
                        <option value="${value.idTipo}">${value.tipo}</option>
                    `);
                });
            }
        }
    });
    $('#ModalAgregarHabitacion').modal('show');
}